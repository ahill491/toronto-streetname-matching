# Toronto Streetname Matching

Use TFIDF to match intersections in Toronto which have Lat/Longitudes to xml text data which only have intersection names. The xml data is rife with typos, and requires fuzzy matching.